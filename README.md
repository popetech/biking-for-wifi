Setup

Latest version is here:

https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/intro.html

Following is notes from what it was on Ubuntu 16.04

    pip install esptool
    
Find USB port for device (Ubuntu 16.04)

Add user to dialout group (to have permission to see/use device)

    sudo usermod -a -G dialout john
    
Logout and back in

Check for device (if unsure, try with device unplugged then plug in to see what is added/removed from list)

    ls /dev/ttyUSB*

Erase flash memory on device

    esptool.py --port /dev/ttyUSB0 erase_flash
    
Download new firmware (latest stable)

http://micropython.org/download#esp8266

Flash to device

    esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 ~/Downloads/esp8266-20171101-v1.9.3.bin
    
Once connected, test with `picocom`

    sudo apt install picocom
    
Connect

    picocom /dev/ttyUSB0 -b115200
    
Hit enter a few times and you will get a basic python interpreter

To quit picocom use `ctrl+a` then `ctrl+q`

OR use screen (laggy...?)

    sudo apt install screen
    sudo screen /dev/ttyUSB0 115200
    
To quit screen use `ctrl+a` then `ctrl+d` (doesn't quite quit...)

Testing input from analog input

    import machine
    from time import sleep
    
    adc = machine.ADC(0)
    
    while True:
        adc.read()
        sleep(.01)
        

        
If you get readings that dip to 0 periodically while pedaling, you are ready to go.

Pushing files to device

https://learn.adafruit.com/micropython-basics-load-files-and-run-code/

boot.py and main.py can be added to the device, boot.py runs only once at startup/reset, main.py runs in a loop, once boot.py is finished.

Using ampy (https://github.com/adafruit/ampy)

    pip install adafruit-ampy
    
Test

    ampy --port /dev/ttyUSB0 ls
    
    OR
    
    export AMPY_PORT=/dev/ttyUSB0
    ampy ls
    
Should see boot.py

Pushing files/reading files

    ampy put main.py
    ampy get main.py
    
Running scripts (file is local, but sent to device to run, output can be observed through REPL, or will return when complete otherwise)

    ampy run --no-output main.py

Wifi Config (boot.py)

    #TODO
