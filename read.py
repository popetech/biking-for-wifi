from machine import Pin, ADC

adc = ADC(0)
led = Pin(2, Pin.OUT)

sensor = adc.read()
led.on()
