from machine import Pin, ADC
from time import sleep

adc = ADC(0)
led = Pin(2, Pin.OUT)

previous_sensor = 0

count = 0
idle = 0

while True:
    sensor = adc.read()
    
    # if we are less than 50, and the difference is greater than 100,
    # we have 1 revolution this prevents double counts for the same cycle
    if sensor < 50 and abs(sensor - previous_sensor) > 100:
        count += 1
        
        # reset idle counter
        idle = 0
        
        # debugging
        print(count)
        
        led.on()
        sleep(.05)
        led.off()
    else:
        idle += 1
        
        if count > 0 and idle > 10000:
            break
        
    previous_sensor = sensor
    sleep(.01)
    
# show score
print("score: "+str(count))
for value in range(0, count):
    led.on()
    sleep(.05)
    led.off()